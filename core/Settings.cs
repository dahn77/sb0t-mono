﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Diagnostics;
using Microsoft.Win32;
using System.IO;

namespace core
{
    public class Settings
    {
		public readonly static String VERSION = "sb0t 5.26 - " + Environment.OSVersion.Platform.ToString() + " " + Environment.OSVersion.Version.ToString();
        public const ushort LINK_PROTO = 500;

        public static bool RUNNING { get; set; }
        public static String WebPath { get; set; }

        public static void Reset()
        {
            externalip = null;
            port = 0;
            name = null;
            language = 0;
            hide_ips = 0;


	        WebPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t");
            WebPath = Path.Combine(WebPath, "Style");

            if (!Directory.Exists(WebPath))
                Directory.CreateDirectory(WebPath);

            DoOnce.Run();
        }

        public static void ScriptCanLevel(bool can)
        {
            Events.ScriptCanLevel(can);
        }

        private static int hide_ips = 0;
        public static bool HideIps
        {
            get
            {
                if (hide_ips == 0)
                    hide_ips = Get<bool>("hide_ips") ? 2 : 1;

                return hide_ips == 2;
            }
        }

        private static IPAddress externalip { get; set; }
        public static IPAddress ExternalIP
        {
            get
            {
                if (externalip != null)
                    return externalip;

                byte[] buffer = Get<byte[]>("ip");

                if (buffer != null)
                    externalip = new IPAddress(buffer);
                else
                    externalip = IPAddress.Loopback;

                return externalip;
            }
            set
            {
                externalip = value;
                Set("ip", externalip.GetAddressBytes());
            }
        }

        private static ushort port { get; set; }
        public static ushort Port
        {
            get
            {
                if (port == 0)
                    port = Get<ushort>("port");

                return port;
            }
        }

        private static String name { get; set; }
        public static String Name
        {
            get
            {
                if (name == null)
                    name = Get<String>("name");

                return name;
            }
        }

        private static byte language { get; set; }
        public static byte Language
        {
            get
            {
                if (language == 0)
                    language = Get<byte>("language");

				
                return language;
            }
        }

        private static String topic { get; set; }
        public static String Topic
        {
            get
            {
                if (topic == null)
                    topic = Get<String>("topic");

                if (topic.Length < 2)
                {
                    topic = "welcome to my room";
                    Set("topic", topic);
                }

                return topic;
            }
            set
            {
                topic = value;
                Set("topic", topic);
            }
        }

        private static Type[] AcceptableTypes = 
        {
            typeof(byte),
            typeof(short),
            typeof(ushort),
            typeof(int),
            typeof(uint),
            typeof(String),
            typeof(byte[]),
            typeof(bool)
        };

		private static Type[] AcceptableTypesNullable = 
        {
            typeof(byte?),
            typeof(short?),
            typeof(ushort?),
            typeof(int?),
            typeof(uint?),
            typeof(String),
            typeof(byte[]),
            typeof(bool?)
        };


        public static T Get<T>(String name, params String[] subkeys)
        {
            String subpath = String.Empty;

            foreach (String str in subkeys)
                subpath += "\\" + str;

            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

            if (key == null)
            {
                Registry.CurrentUser.CreateSubKey("Software\\sb0t\\" + subpath);
                key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);
            }

            if (key != null)
            {
                object value = key.GetValue(name);
                key.Close();
                Type type = typeof(T);

                for (int i = 0; i < AcceptableTypes.Length; i++)
					if (AcceptableTypes[i].Equals(type))
					{
						if (i <= 4)
						{
							if (value != null)
								return (T)Convert.ChangeType(value, typeof(T));
							else
								return (T)Convert.ChangeType(0, typeof(T));
						}
						else if (i == 6)
						{
							if (value != null)
								return (T)Convert.ChangeType(value, typeof(T));
							else
								return default(T);
						}
						else if (i == 5)
						{
							if (value != null)
								return (T)Convert.ChangeType(Encoding.UTF8.GetString((byte[])value), typeof(T));
							else
								return (T)Convert.ChangeType(String.Empty, typeof(T));
						}
						else if (i == 7)
						{
							if (value != null)
								return (T)Convert.ChangeType(((int)value == 1), typeof(T));
							else
								return (T)Convert.ChangeType(false, typeof(T));
						}
					}
            }

            throw new Exception("Registry value not found or invalid casting");
        }

		public static byte? GetNullableByte(String name, params String[] subkeys)
		{
			String subpath = String.Empty;

			foreach (String str in subkeys)
				subpath += "\\" + str;

			RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

			if (key == null)
			{
				Registry.CurrentUser.CreateSubKey("Software\\sb0t\\" + subpath);
				key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

				return null;
			}

			object value = key.GetValue(name);
			key.Close();

			byte? ret = value == null ? null : new byte?((byte)Convert.ChangeType(value, typeof (byte)));
			return ret;

		}

		public static short? GetNullableShort(String name, params String[] subkeys)
		{
			String subpath = String.Empty;

			foreach (String str in subkeys)
				subpath += "\\" + str;

			RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

			if (key == null)
			{
				Registry.CurrentUser.CreateSubKey("Software\\sb0t\\" + subpath);
				key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

				return null;
			}

			object value = key.GetValue(name);
			key.Close();

			short? ret = value == null ? null : new short?((short)Convert.ChangeType(value, typeof(short)));
			return ret;
		}

		public static ushort? GetNullableUshort(String name, params String[] subkeys)
		{
			String subpath = String.Empty;

			foreach (String str in subkeys)
				subpath += "\\" + str;

			RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

			if (key == null)
			{
				Registry.CurrentUser.CreateSubKey("Software\\sb0t\\" + subpath);
				key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

				return null;
			}

			object value = key.GetValue(name);
			key.Close();



			ushort? ret = value == null ? null : new ushort?((ushort)Convert.ChangeType(value, typeof(ushort)));
			return ret;
		}

		public static int? GetNullableInt(String name, params String[] subkeys)
		{
			String subpath = String.Empty;

			foreach (String str in subkeys)
				subpath += "\\" + str;

			RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

			if (key == null)
			{
				Registry.CurrentUser.CreateSubKey("Software\\sb0t\\" + subpath);
				key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

				return null;
			}

			object value = key.GetValue(name);
			key.Close();

			int? ret = value == null ? null : new int?((int)Convert.ChangeType(value, typeof(int)));
			return ret;
		}

		public static uint? GetNullableUInt(String name, params String[] subkeys)
		{
			String subpath = String.Empty;

			foreach (String str in subkeys)
				subpath += "\\" + str;

			RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

			if (key == null)
			{
				Registry.CurrentUser.CreateSubKey("Software\\sb0t\\" + subpath);
				key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath);

				return null;
			}

			object value = key.GetValue(name);
			key.Close();

			uint? ret = value == null ? null : new uint?((uint)Convert.ChangeType(value, typeof(uint)));
			return ret;
		}

        public static bool Set(String name, object value, params String[] subkeys)
        {
			String subpath = String.Empty;

            foreach (String str in subkeys)
                subpath += "\\" + str;

            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath, true);

            if (key == null)
            {
                Registry.CurrentUser.CreateSubKey("Software\\sb0t\\" + subpath);
                key = Registry.CurrentUser.OpenSubKey("Software\\sb0t\\" + subpath, true);
            }

            Type type = value.GetType();

            for (int i = 0; i < AcceptableTypes.Length; i++)
            {
                if (AcceptableTypes[i].Equals(type))
                {
					if (i <= 4)
                    {
                        key.SetValue(name, value, RegistryValueKind.DWord);
                        key.Close();
                        return true;
                    }
                    else if (i == 5)
                    {
                        key.SetValue(name, Encoding.UTF8.GetBytes((String)value), RegistryValueKind.Binary);
                        key.Close();
                        return true;
                    }
                    else if (i == 6)
                    {
                        key.SetValue(name, value, RegistryValueKind.Binary);
                        key.Close();
                        return true;
                    }
                    else if (i == 7)
                    {
                        key.SetValue(name, ((bool)value ? (int)1 : (int)0), RegistryValueKind.DWord);
                        key.Close();
                        return true;
                    }
                }
            }

            key.Close();
            return false;
        }

        public static IPAddress LocalIP
        {
            get
            {
                foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                    if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        return ip;

                return IPAddress.Loopback;
            }
        }
    }
}

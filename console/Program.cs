﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using core;
using System.Threading;
using System.Net;
using System.Web;
using System.Net.Sockets;
using System.IO;
namespace console
{
	public class Program
	{
		private static ServerCore _server = null;
		static void Main(string[] args)
		{
			string pidPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t",
				"sb0t.pid");
			
			_server = new ServerCore();
			ServerCore.LogUpdate += ServerCore_LogUpdate;

			core.Extensions.ExtensionManager.Setup();
			_server.Open();

			var proc = Process.GetCurrentProcess();

			File.WriteAllText(pidPath, proc.Id.ToString(CultureInfo.InvariantCulture));

			while(_server.Running)
			{
				Thread.Sleep(100);
			}
		}

		static string GetLogPath()
		{
			return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t",
				"ServerLog.log");
		}
		static void ServerCore_LogUpdate(object sender, ServerLogEventArgs e)
		{
			StringBuilder sb = new StringBuilder();

			if (e != null)
			{
				sb.AppendLine(e.Message);

				if (e.Error != null)
				{
					sb.AppendLine(e.Error.ToString());
					sb.AppendLine(e.Error.StackTrace);
				}

				sb.AppendLine("----------------------------------------");

				File.AppendAllText(GetLogPath(), sb.ToString());
			}
			
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jurassic;

namespace JSTest
{
	class Program
	{
		static void Main(string[] args)
		{
			var engine = new Jurassic.ScriptEngine();
			Console.Write(engine.Evaluate("1+1").ToString());
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using sb0tUi.Models;

namespace sb0tUi.Controllers
{
    public class AccountController : Controller
    {

		public ActionResult Login(string returnUrl = "")
		{
			if (User.Identity.IsAuthenticated)
				return LogOut();

			//ViewBag.ReturnUrl = returnUrl;
			return View();
		}




	    [HttpPost]
	    [AllowAnonymous]
	    public ActionResult Login(LoginModel model, string returnUrl = "")
	    {
		    if (ModelState.IsValid)
		    {
			    if (Membership.ValidateUser(model.Username, model.Password))
			    {
				    FormsAuthentication.RedirectFromLoginPage(model.Username, model.RememberMe);
			    }
			    ModelState.AddModelError("", "Incorrect username and/or password");
		    }

		    return View(model);
	    }


	    [HttpPost]
		[AllowAnonymous]
		public ActionResult LogOut()
		{
			FormsAuthentication.SignOut();
			return RedirectToAction("Login", "Account", null);
		}
		
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using System.IO;
namespace sb0tUi.Controllers
{
	[Authorize]
    public class ServerController : Controller
    {
        //
        // GET: /Server/StartServer

		[HttpPost]
        public ActionResult Start()
		{
			try
			{
				var sbInfo = new ProcessStartInfo("mono", "bin/console.exe");
				var sbotProcess = Process.Start(sbInfo);

				return Json(new
				{
					Pid = sbotProcess.Id,
					IsRunning = !sbotProcess.HasExited
				});
			}
			catch (Exception e)
			{
				return Json(new
				{
					Pid = -1,
					IsRunning = false,
					Error = e.ToString(),
					Stack = e.StackTrace,
				});
			}
		}

		[HttpPost]
		public ActionResult Stop()
		{
			int pid = GetPid();

			var process = Process.GetProcessById(pid);
			process.Kill();

			RemovePid();

			return new EmptyResult();
		}

		[AllowAnonymous]
		public ActionResult Status()
		{
			int pid = GetPid();
			if (pid == -1)
				return Json(new
				{
					IsRunning = false,
					Pid = -1
				}, JsonRequestBehavior.AllowGet);

			try
			{
				var process = Process.GetProcessById(pid);
				if (process.HasExited == false)
					return Json(new
					{
						IsRunning = true,
						Pid = pid
					}, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Json(new
				{
					Pid = -1,
					IsRunning = false,
					Error = e.ToString(),
					Stack = e.StackTrace,
				}, JsonRequestBehavior.AllowGet);
			}
			
			return Json(new
				{
					IsRunning = false,
					Pid = -1
				}, JsonRequestBehavior.AllowGet);
		}

		private int GetPid()
		{
			string pidPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t",
				"sb0t.pid");

			if (string.IsNullOrWhiteSpace(pidPath) || !System.IO.File.Exists(pidPath))
				return -1;

			var pidText = System.IO.File.ReadAllText(pidPath);
			var pid = int.Parse(pidText);

			return pid;
		}

		private void RemovePid()
		{
			string pidPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t",
				"sb0t.pid");

			if (string.IsNullOrWhiteSpace(pidPath) || !System.IO.File.Exists(pidPath))
				return;

			System.IO.File.Delete(pidPath);
		}
    }
}

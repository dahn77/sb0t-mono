﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Threading;

namespace sb0tUi
{
	public class ApplicationScheduler
	{
		static Queue<Task> taskQueue = null;
		static ManualResetEvent resetEvent = null;
		static Thread thread = null;
		static bool running = false;
		object lockObj = new object();
		
		static ApplicationScheduler()
		{
			taskQueue = new Queue<Task>();
			resetEvent = new ManualResetEvent(false);
			thread = new Thread(ThreadLoop);
		}

		public static void Start()
		{
			running = true;
			thread.Start();
		}

		public static void Stop()
		{
			running = false;
		}

		public static void QueueTask(Task task)
		{
			taskQueue.Enqueue(task);
			resetEvent.Set();
		}

		private static void ThreadLoop()
		{
			while (running)
			{
				resetEvent.WaitOne();
				Task t = null;

				if (taskQueue.Count > 0)
					t = taskQueue.Dequeue();

				if (taskQueue.Count == 0)
					resetEvent.Reset();

				if (t != null)
					t.Start();
			}
		}
	}
}
﻿/// <reference path="../jquery-1.8.2.js" />
/// <reference path="../knockout-2.2.0.js" />
/// <reference path="../jquery.ajax-retry.min.js" />

ko.observableArray.fn.filterByProperty = function (propName, matchValue) {
    return ko.computed(function () {
        var allItems = this(), matchingItems = [];
        for (var i = 0; i < allItems.length; i++) {
            var current = allItems[i];
            if (ko.toJS(current[propName]) === matchValue)
                matchingItems.push(current);
        }
        return matchingItems;
    }, this);
};

var DropDownSelectObject = function(id, name) {
    this.id = id;
    this.name = name;
};

function SbotSetting(setting) {
    var self = this;

    self.name = ko.observable(setting.Name);

    if (typeof (setting.Label) == "object") {
        self.label = [];
        for (var i in setting.Label) {
            self.label.push(new DropDownSelectObject(i, setting.Label[i]));
            for (var j = 0; j < self.label.length; j++) {
                if(setting.Value == self.label[j].id)
                    self.value = ko.observable(self.label[j]);
            }
            
        }
    } else {
        self.label = ko.observable(setting.Label);
        self.value = ko.observable(setting.Value);
    }
    
    self.section = ko.observable(setting.Section);
}

function SbotSettingsViewModel() {
    var self = this;

    // Settings
    self.settings = ko.observableArray([]);
    self.settingsStrings = ko.observableArray([]);
    self.settingsBooleans = ko.observableArray([]);
    self.settingsNumbers = ko.observableArray([]);
    self.settingsObjects = ko.observableArray([]);

    // UI Settings
    self.navSections = ["Settings", "Server Log"];
    self.navSubSections = ["Main", "Admin", "Advanced", "Scripting", "Captcha", "Restrictions", "Ban List"];

    self.navSectionCurrent = ko.observable();
    self.navSubSectionCurrent = ko.observable();

    self.isServerRunning = ko.observable(false);
    self.btnStartStopText = ko.computed(function() {
        if (self.isServerRunning())
            return "Server Running (Click to stop)";
        else
            return "Server Stoped (Click to run)";
    });

    // Behaviours
    self.changeNavSection = function(section) {
        self.navSectionCurrent(section);
    };

    self.changeNavSubSection = function(section) {
        self.navSubSectionCurrent(section);
        self.loadSettings(self.navSubSections.indexOf(section), "string");
        self.loadSettings(self.navSubSections.indexOf(section), "boolean");
        self.loadSettings(self.navSubSections.indexOf(section), "object");
    };

    self.loadSettings = function(section, type) {
        var url = '/api/SbotSettings?section=' + section + '&type=' + type;
        $.ajax({
            url: url,
            dataType: "json",
            success: function(settings) {
                if (type == "string") {
                    self.settingsStrings.removeAll();
                    $.each(settings, function(index, item) {
                        var obj = new SbotSetting(item);

                        obj.value.subscribe(function(newValue) {
                            $.post('/api/SbotSettings', this, null);
                        }, obj);

                        self.settingsStrings.push(obj);
                    });
                }
                if (type == "boolean") {
                    self.settingsBooleans.removeAll();
                    $.each(settings, function(index, item) {
                        var obj = new SbotSetting(item);

                        obj.value.subscribe(function(newValue) {
                            $.post('/api/SbotSettings', this, null);
                        }, obj);

                        self.settingsBooleans.push(obj);
                    });
                }
                if (type == "object") {
                    self.settingsObjects.removeAll();
                    if (settings != null) {
                        $.each(settings, function(index, item) {
                            var obj = new SbotSetting(item);

                            obj.value.subscribe(function(newValue) {


                                if (typeof (newValue) == "object") {
                                    var data =
                                    {
                                        name: obj.name(),
                                        label: obj.label,
                                        section: obj.section(),
                                        value: obj.value().id
                                    };

                                    $.post('/api/SbotSettings', data, null);
                                }
                            }, obj);

                            self.settingsObjects.push(obj);
                        });
                    }
                }
            }
        }).retry({ times: 3 });
    };


        self.changeNavSection(self.navSections[0]);
        self.changeNavSubSection(self.navSubSections[0]);
};